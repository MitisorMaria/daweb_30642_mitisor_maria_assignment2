import flask
from flask_mail import Mail, Message
from flask import request

app = flask.Flask(__name__)
app.config["DEBUG"] = True

mail_settings = {
    "MAIL_SERVER": 'smtp.gmail.com',
    "MAIL_PORT": 465,
    "MAIL_USE_TLS": False,
    "MAIL_USE_SSL": True,
    "MAIL_USERNAME": 'belleartis.recrutare2020',
    "MAIL_PASSWORD": 'ezfrlsrvqmqdakdp'
}

app.config.update(mail_settings)
mail = Mail(app)


@app.route('/', methods=['POST'])
def sendMail():
    if __name__ == '__main__':
        with app.app_context():
            msg = Message(subject=request.args['subject'],
                          sender=app.config.get("MAIL_USERNAME"),
                          recipients=[request.args['addr']],
                          body=request.args['body'])
            mail.send(msg)
        return "Done"

@app.route('/', methods=['GET'])
def home():
    return "<h1>App backend</h1>"

app.run()