import React from 'react'
import logo from './commons/images/sigla.png';


import {
    Nav,
    Navbar,
    NavbarBrand,
    NavLink
} from 'reactstrap';

import counterpart from "counterpart";
import Translate from "react-translate-component";
import eng from "./lang/eng";
import ro from "./lang/ro";

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

counterpart.registerTranslations('eng', eng);
counterpart.registerTranslations('ro', ro);

class NavigationBar extends React.Component {
    state = {
    };

    componentDidMount() {
        const lang = localStorage.getItem("lang");
        if(lang) {
            this.setState({ lang: lang});
            counterpart.setLocale(lang);
        }
    }
    componentDidUpdate(prevProps, prevState) {

    }

    onLangChange = (e) => {
        this.setState({lang: e.target.value});
        localStorage.setItem("lang", e.target.value);
        counterpart.setLocale(localStorage.getItem("lang"));
    }

    render() {
        return (
            <div>
                <Navbar color="dark" light expand="md">
                    <NavbarBrand href="/">
                        <img src={logo} width={"50"}
                             height={"35"} />
                    </NavbarBrand>

                    <Nav className="mr-auto" navbar>
                        <ul style={textStyle} className='nav navbar-nav navbar-inverse navbar-custom'>
                            <li><Translate content="acasa" component="a" class="nav-link" href="/acasa" style={textStyle} unsafe={true}/></li>
                            <li><Translate content="noutati" component="a" class="nav-link" href="/noutati" style={textStyle} unsafe={true}/></li>
                            <li><Translate content="despre" component="a" class="nav-link" href="/despre" style={textStyle} unsafe={true}/></li>
                            <li><Translate content="profil" component="a" class="nav-link" href="/profilStudent" style={textStyle} unsafe={true}/></li>
                            <li><Translate content="coord" component="a" class="nav-link" href="/coordonator" style={textStyle} unsafe={true}/></li>
                            <li><NavLink href="/contact" style={textStyle}>Contact</NavLink></li>
                        </ul>
                    </Nav>
                    <select  value={this.state.lang} onChange={this.onLangChange}>
                        <option value="eng">ENG</option>
                        <option value="ro">RO</option>
                    </select>
                </Navbar>
            </div>

        )
    }
}

export default NavigationBar
