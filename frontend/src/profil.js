import React from 'react';
//import {View} from 'react-native';
import BackgroundImg from "./commons/images/poza.jpg";
import style from './commons/styles/style.css'
const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`
};

const textStyle = {color: 'white', align: 'middle'};
const imgStyle = {display: 'flex-end', align: 'right'};
class Profil extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
    }



    refresh(){
        this.forceUpdate()
    }

    render() {
        return (
            <div  style={backgroundStyle}>
                <h1 style={textStyle}>Profil student</h1>

                    <p style={imgStyle}><img src ={require ('./commons/images/maria.jpg') }/></p>
                        <p style={textStyle}><b>Nume și prenume: </b>Mitișor Maria-Alexandra</p>
                        <p style={textStyle}><b>Tehnologii de interes: </b>
                            <ul>
                                <li>Java Spring Framework</li>
                                <li>Python (TensorFlow, Keras, Django)</li>
                                <li>JS, React</li>
                            </ul>
                        </p>

                <footer>
                    <p className="copyright">© Maria Mitișor 2020</p>
                </footer>
            </div>
        );
    };

}

export default Profil;
