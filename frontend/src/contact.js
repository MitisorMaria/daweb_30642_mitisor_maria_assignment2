import React from 'react';
import counterpart from 'counterpart';
import BackgroundImg from './commons/images/poza.jpg';
import Translate from 'react-translate-component';
import eng from './lang/eng';
import ro from './lang/ro';
import axios from 'axios';
import {HOST} from './commons/hosts';


import './person-data/person/fields/fields.css';


const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`,
    display: 'flex',
    flexDirection: 'column'
};

const textStyle = {color: 'white', align: 'middle'};

counterpart.registerTranslations('eng', eng);
counterpart.registerTranslations('ro', ro);
counterpart.setLocale(localStorage.getItem("lang"));



class Contact extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null,
            lang: "ro",
        };
        
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    componentDidMount() {
        const lang = localStorage.getItem("lang");
        if(lang) {
            this.setState({ lang: lang});
            counterpart.setLocale(lang);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const lang = localStorage.getItem("lang");
        counterpart.setLocale(lang);
    }



    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit(event) {
        const email = this.state.mail;
        const subj = this.state.subject;
        const cont = this.state.content;
        axios.post(HOST.backend_api, null, { params: {
                addr: email,
                subject: subj,
                body: cont
            }})
            .then(response => response.status)
            .catch(err => console.warn(err));
    }

    render() {
        return (
            <div  style={backgroundStyle}>
                <Translate content="titleContact" component="h1" style={textStyle}/>
                <Translate content="address" component="p" style={textStyle} unsafe={true}/>
                <Translate content="phone" component="p" style={textStyle} unsafe={true}/>
                <Translate content="mail" component="p" style={textStyle} unsafe={true}/>

                <form onSubmit={this.handleSubmit}>
                    <Translate content="message" component="p" style={textStyle}/>
                    <label>
                        <input type="text" name="mail" value={this.state.mail}  placeholder={"Enter e-mail address"} onChange={this.handleChange} />
                    </label>


                    <label>
                        <input type="text" name="subject" value={this.state.subject}  placeholder={"Enter subject"} onChange={this.handleChange} />
                    </label>

                    <label>
                        <input type="text" name="content" value={this.state.content}  placeholder={"Enter message"} onChange={this.handleChange} />
                    </label>


                    <input type="submit" value="Submit" />
                </form>


                <footer>
                    <p className="copyright">© Maria Mitișor 2020</p>
                </footer>
            </div>
        );
    };

}

export default Contact;
