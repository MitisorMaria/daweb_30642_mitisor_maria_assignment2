export default {
    titleContact: 'Contact',
    address: '<b>Adresa: </b>str. George Barițiu 26-28, Cluj-Napoca',
    phone: '<b>Nr. de telefon: </b>0767-356777',
    mail: '<b>Adresa de e-mail: </b>maria.mitisor@gmail.com',
    noutati: 'Noutăți',
    acasa: 'Acasă',
    despre: 'Despre lucrare',
    profil: 'Profil student',
    coord: 'Coordonator',
    message: 'Lăsați un mesaj:',
    email: 'Adresa dvs. de mail: ',
    subject: 'Subiect:',
    messageContent: 'Mesaj:'
}