export default {
    titleContact: 'Contact',
    address: '<b>Address: </b>26-28 George Baritiu Street, Cluj-Napoca',
    phone: '<b>Phone nr: </b>0767-356777',
    mail: '<b>E-mail address: </b>maria.mitisor@gmail.com',
    noutati: 'News',
    acasa: 'Home',
    despre: 'About',
    profil: 'Student profile',
    coord: 'Coordinator',
    message: 'Leave a message: ',
    email: 'Your e-mail address: ',
    subject: 'Subject:',
    messageContent: 'Message:'
}