import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import Noutati from './noutati'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import Despre from "./despre";
import Coordonator from "./coordonator";
import Profil from "./profil";
import Contact from "./contact";
import Acasa from "./acasa";


let enums = require('./commons/constants/enums');

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Acasa/>}
                        />

                        <Route
                            exact
                            path='/acasa'
                            render={() => <Acasa/>}
                        />

                        <Route
                            exact
                            path='/despre'
                            render={() => <Despre/>}
                        />

                        <Route
                            exact
                            path='/profilStudent'
                            render={() => <Profil/>}
                        />

                        <Route
                            exact
                            path='/coordonator'
                            render={() => <Coordonator/>}
                        />

                        <Route
                            exact
                            path='/contact'
                            render={() => <Contact/>}
                        />

                        <Route
                            exact
                            path='/noutati'
                            render={() => <Noutati/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
